from django.contrib import admin
from WEVAPI.models import *
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export.fields import Field


# Register your models here.
class ParticipantResource(resources.ModelResource):
    modules_name = Field(attribute='get_modules', column_name="modules")

    class Meta:
        exclude = ('modules', 'id', 'annee_etude',)
        model = Participant
        export_order = ('nom', 'prenom', 'mail', 'num_etudiant', 'tel', 'departement', 'assos', 'modules_name')


@admin.register(Participant)
class ParticipantAdmin(ImportExportModelAdmin):
    resource_class = ParticipantResource
    list_display = ('nom', 'prenom', 'mail', 'num_etudiant', 'get_modules')


@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    list_display = ('intitule', 'creneau', 'intervenant', 'couleur', 'get_places_dispo')


admin.site.register(Creneau)
