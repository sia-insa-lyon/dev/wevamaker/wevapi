from django.db import models
import datetime


class Creneau(models.Model):
    debut = models.DateTimeField(verbose_name="Début du créneau")
    fin = models.DateTimeField(verbose_name="Fin du créneau")
    inscription_open = models.BooleanField(verbose_name="Inscriptions ouvertes", default=False)

    def formatted(self):
        days = {
            8: "Jeudi",
            9: "Vendredi",
            10: "Samedi",
            11: "Dimanche"
        }

        debut = self.debut + datetime.timedelta(hours=2)
        fin = self.fin + datetime.timedelta(hours=2)

        if debut.day in days.keys():
            return days[debut.day] + " " + str(debut.day) + "/" + str(debut.month) + " de " + debut.strftime(
                "%H:%M") + " à " + fin.strftime("%H:%M")

        return str(debut.day) + "/" + str(debut.month) + " de " + debut.strftime(
            "%H:%M") + " à " + fin.strftime("%H:%M")

    def __str__(self):
        return self.formatted()


class Module(models.Model):
    creneau = models.ForeignKey(Creneau, related_name="module", on_delete=models.CASCADE)

    intitule = models.CharField(verbose_name="Intitulé du module", max_length=150)
    intervenant = models.CharField(verbose_name="Intervenant du module", max_length=100, default="")
    description = models.TextField(verbose_name="Descrption détaillée du module")
    couleur = models.CharField(verbose_name="Couleur du module",
                               choices=(('Noir', 'Noir'), ('Vert', 'Vert'), ('Jaune', 'Jaune'), ('Bleu', 'Bleu')), max_length=30)

    places_max = models.PositiveIntegerField(verbose_name="Quota de personnes", default=0)
    places_prises = models.PositiveIntegerField(verbose_name="Nombre de places occupées", default=0)

    def get_places_dispo(self):
        return str(self.places_max-self.places_prises)

    def __str__(self):
        return self.intitule


class Participant(models.Model):
    nom = models.CharField(verbose_name="Nom", max_length=50)
    prenom = models.CharField(verbose_name="Prénom", max_length=50)
    mail = models.EmailField(verbose_name="Adresse email", unique=True)
    tel = models.CharField(verbose_name="Téléphone", blank=True, max_length=50)

    num_etudiant = models.CharField(verbose_name="Numéro étudiant", blank=True, max_length=50, unique=True)
    annee_etude = models.CharField(verbose_name="Année d'étude",
                                   choices=[("1A", "1A"), ("2A", "2A"), ("3A", "3A"), ("4A", "4A"), ("5A", "5A"),
                                            ("6A+", "6A+"), ("Alumni", "Alumni"), ("Autre", "Autre")], max_length=10)
    departement = models.CharField(verbose_name="Département",
                                   choices=[("BS", "BS"), ("GE", "GE"), ("GI", "GI"), ("GEN", "GEN"), ("IF", "IF"),
                                            ("TC", "TC"), ("GCU", "GCU"), ("GM", "GM"), ("SGM", "SGM"),
                                            ("FIMI", "FIMI"), ("Autre", "Autre")],
                                   max_length=10)

    assos = models.CharField(verbose_name="Asso représentée", blank=True, max_length=200)

    modules = models.ManyToManyField(Module, related_name="participant", blank=True)

    def get_modules(self):
        return ", ".join([str(p) for p in self.modules.all()])

    def __str__(self):
        return self.nom + " " + self.prenom
