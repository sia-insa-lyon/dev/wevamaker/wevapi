import requests
from django.shortcuts import get_object_or_404, render
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework import filters
from django.http import QueryDict, HttpResponse
from rest_framework.viewsets import GenericViewSet

from WEVAmaker import settings
from .serializers import ModuleSerializer, ParticipantSerializer, CreneauSerializer, ModuleIdSerializer
from .models import Module, Creneau, Participant


class ModuleView(viewsets.ReadOnlyModelViewSet):
    serializer_class = ModuleSerializer
    queryset = Module.objects.all()


class CreneauView(viewsets.ReadOnlyModelViewSet):
    ordering_fields = ['debut', 'fin']
    ordering = ['debut']
    filter_backends = [filters.OrderingFilter]
    serializer_class = CreneauSerializer
    queryset = Creneau.objects.all()


class ParticipantView(mixins.CreateModelMixin, GenericViewSet):
    serializer_class = ParticipantSerializer
    queryset = Participant.objects.all()

    def create(self, request, *args, **kwargs):
        print(request.data)

        if type(request.data) is not dict:
            data = request.data.dict()
        else:
            data = request.data.copy()  # Creates a mutable copy of the received data

        if not validate_captcha(request.POST['h-captcha-response']):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data={"message": "Erreur de validation du captcha. Rechargez la page s'il apparaît valide"})

        if not 'modules' in data.keys():
            data['modules'] = []

            for cre in Creneau.objects.all():
                keyname = 'creneau' + str(cre.pk)

                if keyname in data.keys() and data[keyname] != 'null':
                    moduleId = data[keyname]
                    module = Module.objects.get(pk=moduleId)

                    if module.places_prises < module.places_max:
                        data['modules'].append(int(moduleId))
                        module.places_prises += 1
                        module.save()

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


@api_view(('POST',))
def UpdateModules(request):
    if not validate_captcha(request.POST['h-captcha-response']):
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data={"message": "Erreur de validation du captcha. Rechargez la page s'il apparaît valide"})

    if type(request.data) is not dict:
        data = request.data.dict()
    else:
        data = request.data.copy()  # Creates a mutable copy of the received data

    if 'email' in data.keys() and 'num_etudiant' in data.keys():
        participant = Participant.objects.filter(mail=data['email']).filter(num_etudiant=data['num_etudiant'])

        if len(participant) == 1:  # If email corresponds to student number
            participant = participant[0]
            data['modules'] = []

            # Remove all places taken by previously selected modules
            for module in participant.modules.all():
                module.places_prises -= 1
                module.save()

            for i in range(1, Creneau.objects.latest('pk').pk):
                keyname = 'creneau' + str(i)
                if keyname in data.keys() and data[keyname] != 'null':
                    moduleId = data[keyname]
                    module = Module.objects.get(pk=moduleId)

                    if module.places_prises < module.places_max:
                        data['modules'].append(int(moduleId))
                        module.places_prises += 1
                        module.save()

            serializer = ParticipantSerializer(participant, data=data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            return Response(status=status.HTTP_202_ACCEPTED)
    return Response(status=status.HTTP_404_NOT_FOUND,
                    data={"message": "Participant non trouvé. Vérifiez votre mail + numéro étudiant"})


@api_view(('GET',))
def SearchParticipant(request):
    if 'mail' in request.GET.keys() and 'num' in request.GET.keys():
        participant = get_object_or_404(Participant, mail=request.GET['mail'])

        if participant.num_etudiant != request.GET['num']:
            return Response(status=status.HTTP_404_NOT_FOUND)

        modules = participant.modules.all()
        serializer = ModuleIdSerializer(modules, many=True)

        return Response(serializer.data)

    return Response(status=status.HTTP_404_NOT_FOUND)


def validate_captcha(token):
    """
    Verifies the HCaptcha attached to the form
    Read the docs for more information : https://docs.hcaptcha.com/

    :param token: token provided by the form
    :return: True if HCaptcha validates the captcha
    """

    params = {
        "secret": settings.HCAPTCHA_PRIVATE_KEY,
        "response": token
    }

    captcha_result = requests.post("https://hcaptcha.com/siteverify", params)
    return captcha_result.json()['success']


def console(request):
    # TODO
    if (request.user.is_superuser):
        modules = Module.objects.all()
        return render(request, template_name='console.html', context={'modules': modules})
    return HttpResponse(status=403)


def export_view(request):
    if request.method == 'POST':
        if (request.user.is_superuser and 'module' in request.POST.keys()):
            module = get_object_or_404(Module, id=request.POST['module'])
            participants = Participant.objects.filter(modules__id=module.id)
            return render(request, template_name='module_export.html',
                          context={'participants': participants, 'module': module})

    return HttpResponse(status=403)


def healthcheck(request):
    return HttpResponse(status=200)
