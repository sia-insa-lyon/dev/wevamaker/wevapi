from rest_framework import serializers
from .models import Creneau, Participant, Module

class ModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = ('pk', 'creneau', 'intervenant', 'intitule', 'description', 'couleur', 'places_max', 'places_prises')

class ModuleIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = ['pk']

class CreneauSerializer(serializers.ModelSerializer):
    formated = serializers.ReadOnlyField(source='formatted')

    class Meta:
        model = Creneau
        fields = ('pk', 'debut', 'fin', 'inscription_open', 'formated')

class ParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = ('nom', 'prenom', 'mail', 'tel', 'num_etudiant', 'annee_etude', 'departement', 'assos', 'modules')
