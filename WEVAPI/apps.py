from django.apps import AppConfig


class WevapiConfig(AppConfig):
    name = 'WEVAPI'
