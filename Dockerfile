FROM python:3.8

RUN pip install --upgrade pip

WORKDIR /app
COPY requirements.txt /app
RUN pip3 install -r requirements.txt
COPY . /app
VOLUME /app/staticfiles

RUN chmod +x /app/bash/run-prod.sh
CMD /app/bash/run-prod.sh
