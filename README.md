# WEVAPI

Le backend de WEVAmaker, application web pour l'inscription et la gestion des modules du WEVA


## Install
```bash
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python manage.py migrate

echo "Eh hop !"
docker run --rm --name database -it -v ~/.db-postgre_weva:/var/lib/postgresql/data -p 5432:5432 -e POSTGRES_USER=user -e POSTGRES_DB=${PWD}/db -e POSTGRES_PASSWORD=password postgres
APP_DEBUG=True python manage.py runserver
```

## Install w/ Docker
```

```
