"""WEVAmaker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from WEVAPI import views

router = routers.DefaultRouter()
router.register(r'modules', views.ModuleView, 'module')
router.register(r'participants', views.ParticipantView, 'participant')
router.register(r'creneau', views.CreneauView, 'creneau')


urlpatterns = [
    path('api/admin/', admin.site.urls),
    path('api/admin/console', views.console),
    path('api/admin/export', views.export_view),
    path('api/', include(router.urls)),
    path('api/reservation/update/', views.UpdateModules),
    path('api/search', views.SearchParticipant),
    path('healthcheck', views.healthcheck)
]
